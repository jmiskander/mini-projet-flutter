import 'package:flutter/material.dart';
import 'package:sportsapp/screens/welcome_screen.dart';
import 'package:sportsapp/splash_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Jam3iti',
      theme: ThemeData.dark(),
      //home: WelcomeScreen(),
      home: SplashPage(),
        routes: {
          '/home': (context) => WelcomeScreen(),
       }
    );
  }
}

