import 'package:firebase_auth/firebase_auth.dart';
import 'package:sportsapp/models/user.dart';


class AuthService
{

  final FirebaseAuth _auth = FirebaseAuth.instance;

  //create a user ibject based on firebase user

  User _userFromFireBase(FirebaseUser user)
  {
    return user != null ? User(uid: user.uid) : null;
  }


  // auth change user stream

  Stream<User> get user
  {
    return _auth.onAuthStateChanged
        .map(_userFromFireBase);
  }


  // sign in anonymosly

  Future signInAnon() async
  {
    try{
      AuthResult result = await _auth.signInAnonymously();
      FirebaseUser user = result.user;
      return _userFromFireBase(user);
    } catch (e)
    {
      print(e.toString());
      return null;
    }
  }

  // sign in with email and password

  // register with email and password

Future registerWithEmailAnPassword(String email, String password) async
{
  try
  {
    AuthResult result = await _auth.createUserWithEmailAndPassword(email: email, password: password);
    FirebaseUser user = result.user;

    return _userFromFireBase(user);
  }
  catch(e)
  {
    print(e.toString());
    return null;
  }
}

  // sign out
}