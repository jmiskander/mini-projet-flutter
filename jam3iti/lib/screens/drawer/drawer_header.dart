import 'package:flutter/material.dart';


class HeaderDrawer extends StatefulWidget {

  @override
  _HeaderDrawerState createState() => _HeaderDrawerState();
}

class _HeaderDrawerState extends State<HeaderDrawer> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.black12,
      width: double.infinity,
      height: 200,
      padding: EdgeInsets.only(top: 20.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            margin: EdgeInsets.only(bottom: 10),
            height: 70,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              image: DecorationImage(
                image: AssetImage('assets/images/1919.png'),
              ),
            ),
          ),
          Text(
            "User",
            style: TextStyle(
                color: Colors.white70,
                fontSize: 20,
            ),
          ),
          Text(
            "test@test.com",
            style: TextStyle(
              color: Colors.amber,
              fontSize: 14,
            ),
          ),
        ],
      ),
    );
  }
}
