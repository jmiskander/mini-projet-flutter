import 'package:flutter/material.dart';
import 'package:sportsapp/HomeScreen.dart';
import 'package:sportsapp/MainScreen.dart';
import 'package:sportsapp/screens/components/authenticate/already_have_an_account_check.dart';
import 'package:sportsapp/screens/components/authenticate/login/login_background.dart';
import 'package:sportsapp/screens/components/or_divider.dart';
import 'package:sportsapp/screens/components/my_buttons_inputs_fields/rounded_button.dart';
import 'package:sportsapp/screens/components/my_buttons_inputs_fields/rounded_input_field.dart';
import 'package:sportsapp/screens/components/my_buttons_inputs_fields/rounded_password_field.dart';
import 'package:sportsapp/screens/components/authenticate/signup/social_icon.dart';
import 'package:sportsapp/screens/components/authenticate/signup/signup_screen.dart';
import 'package:sportsapp/services/auth.dart';



class LoginBody extends StatefulWidget {
  const LoginBody({
    Key key,
}) : super(key: key);

  @override
  State<LoginBody> createState() => _LoginBodyState();
}

class _LoginBodyState extends State<LoginBody> {


  // tracking fields states

  String email = '';
  String password = '';


  final AuthService _auth = AuthService();

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return LoginBackground(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget> [
            Text(
              "LOGIN",
              style: TextStyle(
                fontWeight: FontWeight.bold
              ),
            ),
            SizedBox(
              height: size.height * 0.03,
            ),
            Image.asset(
              "assets/images/oulidha.png",
              height: size.height * 0.25,
            ),
            SizedBox(
              height: size.height * 0.01,
            ),
            RoundedInputField(
              hintText: "Your Email / Username",
              onChanged: (value) {
                setState(() => email = value);
              },
            ),
            RoundedPasswordField(
              onChanged: (value) {
                setState(() => password = value);
              },
            ),
            RoundedButton(
              color: Colors.red,
              textColor: Colors.black,
              text: "LOGIN",
              press: ()  {
                Navigator.push(
                    context, MaterialPageRoute(
                    builder: (context){
                    return MainScreen();},
                  ),
                );
              },
            ),
            SizedBox(
              height: size.height * 0.01,
            ),
            AlreadyHaveAnAccountCheck(
              press: () {
                Navigator.push(
                  context, MaterialPageRoute(
                  builder: (context){
                    return SignUpScreen();
                    },
                ),
                );},
            ),
            OrDivider(),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget> [
                SocialIcon(
                  iconSrc: "assets/images/fb.png",
                  press: () {},
                ),
                SocialIcon(
                  iconSrc: "assets/images/google.png",
                  press: () {},
                ),

              ],
            ),
          ],
        ),
      ),
    );
  }
}

