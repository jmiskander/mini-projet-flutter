import 'dart:io';
import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:sportsapp/MainScreen.dart';
import 'package:sportsapp/screens/components/authenticate/already_have_an_account_check.dart';
import 'package:sportsapp/screens/components/my_buttons_inputs_fields/rounded_button.dart';
import 'package:sportsapp/screens/components/my_buttons_inputs_fields/rounded_confirm_password_field.dart';
import 'package:sportsapp/screens/components/my_buttons_inputs_fields/rounded_input_field.dart';
import 'package:sportsapp/screens/components/my_buttons_inputs_fields/rounded_password_field.dart';
import 'package:sportsapp/screens/components/authenticate/signup/signup_background.dart';
import 'package:sportsapp/screens/components/authenticate/login/login_screen.dart';
import 'package:sportsapp/services/auth.dart';


class SignUpBody extends StatefulWidget {
  final Widget child;

  const SignUpBody({
    Key key,
    @required this.child,
  }) : super(key: key);

  @override
  State<SignUpBody> createState() => _SignUpBodyState();
}

class _SignUpBodyState extends State<SignUpBody> {

  final _formKey = GlobalKey<FormState>();

  // tracking fields states

  String email = '';
  String password = '';
  String confPass ='';

  String error ='';


  final AuthService _auth = AuthService();

  // the selected image (photo) from camera or gallery
  File _image;

  Future getImageFromCam() async
  {
    var image = await ImagePicker.pickImage(source: ImageSource.camera);

    setState(() {
      _image = image;
    });
  }

  Future getImageFromGall() async
  {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);

    setState(() {
      _image = image;
    });
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return SignUpBackground(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget> [
            Text(
              "SIGN UP",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(
              height: size.height * 0.001,
            ),
            Image.asset(
              "assets/images/oulidha.png",
              height: size.height * 0.2,
            ),
            Form(
              key: this._formKey,
              child: Column(
                children: <Widget>[
                  RoundedInputField(
                    hintText: "Your full name",
                    onChanged: (value) {},
                    validator: (value) => value.isEmpty ? 'Enter your full name' : null,
                  ),
                  RoundedInputField(
                    hintText: "Your email",
                    icon: Icons.email,
                    onChanged: (value) {
                      setState(() => email = value);},
                    validator: (value) => value.isEmpty ? 'Enter your mail' : null,
                  ),
                  RoundedPasswordField(
                    onChanged: (value) {
                      setState(() => password = value);},
                    validator: (value) => value.length < 6 ? 'Password must be at least 6 characters' : null,
                  ),
                  RoundedConfPasswordField(
                    onChanged: (value) {
                      setState(() => confPass = value);
                    },
                    validator: (value) => value.isEmpty ? 'Enter your password confirmation' : null,
                  ),
                  SizedBox(
                    height: size.height * 0.01,
                  ),
                  SizedBox(
                    width: 10.0,
                  ),

                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      FloatingActionButton(
                        onPressed: getImageFromCam,
                        //tooltip: 'Take a Photo',
                        backgroundColor: Colors.amber,
                        child: Icon(Icons.add_a_photo),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      FloatingActionButton(
                        onPressed: getImageFromGall,
                        //tooltip: 'Select a photo',
                        backgroundColor: Colors.amber,
                        child: Icon(Icons.wallpaper),
                      ),
                      Column(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children:<Widget> [
                            Container(
                              width: 200.0,
                              child: _image == null ? Text("No Image Selected !") : Image.file(_image),
                            ),
                          ]
                      ),
                      SizedBox(
                        height: 1.0,
                      ),
                    ],
                  ),
                  SizedBox(
                    height: size.height * 0.01,
                  ),
                  RoundedButton(
                    color: Colors.red,
                    text: "Register",
                    textColor: Colors.black,
                    press: () async
                    {
                      if(_formKey.currentState.validate())
                        {
                          dynamic result = await this._auth.registerWithEmailAnPassword(email, password);
                          if(result == null)
                            {
                              setState(() => error = 'Connexion with Firebase error');
                            }
                         Navigator.push(
                             context,
                             MaterialPageRoute(
                                 builder: (context){
                                   return MainScreen();
                                 },
                                 ));
                        }
                    },
                  ),
                ],
              ),
            ),



            AlreadyHaveAnAccountCheck(
              login: false,
              press: (){
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context)
                        {
                          return LoginScreen();
                        },
                    ),
                );
              },
            ),

          ],
        ),
      ),
    );
  }
}

