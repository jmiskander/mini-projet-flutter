import 'package:flutter/material.dart';
import 'package:sportsapp/screens/components/my_buttons_inputs_fields/text_field_container.dart';

class RoundedConfPasswordField extends StatefulWidget {

  final ValueChanged<String> onChanged;
  final Function validator;

  const RoundedConfPasswordField({
    Key key,
    this.onChanged,
    this.validator,
    
  }) : super(key: key);

  @override
  State<RoundedConfPasswordField> createState() => _RoundedConfPasswordFieldState();
}

class _RoundedConfPasswordFieldState extends State<RoundedConfPasswordField> {
  bool _showPassword = false;
  @override
  Widget build(BuildContext context) {
    return TextFieldContainer(
      child: TextField(
        style: TextStyle(
          color: Colors.black,
        ),
        obscureText: ! _showPassword,
        onChanged: widget.onChanged,
        decoration: InputDecoration(
          hintText: "Confirm Password",
          hintStyle: TextStyle(color: Colors.black),
          icon: Icon(
            Icons.lock,
            color: Colors.black,
          ),
          suffixIcon: GestureDetector(
            onTap: (){
              setState((){
                _showPassword = ! _showPassword;
              });
            },
            child: Icon(
              _showPassword ? Icons.visibility : Icons.visibility_off,
              color: Colors.black,
            ),
          ),
          border: InputBorder.none,
        ),
      ),
    );
  }
}
