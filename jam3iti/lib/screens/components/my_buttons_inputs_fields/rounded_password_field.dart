import 'package:flutter/material.dart';
import 'package:sportsapp/screens/components/my_buttons_inputs_fields/text_field_container.dart';

class RoundedPasswordField extends StatefulWidget {

  final ValueChanged<String> onChanged;

  final Function validator;

  const RoundedPasswordField({
    Key key,
    this.onChanged,
    this.validator,
    
  }) : super(key: key);

  @override
  State<RoundedPasswordField> createState() => _RoundedPasswordFieldState();
}

class _RoundedPasswordFieldState extends State<RoundedPasswordField> {
  bool _showPassword = false;
  @override
  Widget build(BuildContext context) {
    return TextFieldContainer(
      child: TextFormField(
        style: TextStyle(
          color: Colors.black,
        ),
        obscureText: ! _showPassword,
        onChanged: widget.onChanged,
        decoration: InputDecoration(
          hintText: "Password",
          hintStyle: TextStyle(color: Colors.black),
          icon: Icon(
            Icons.lock,
            color: Colors.black,
          ),
          suffixIcon: GestureDetector(
            onTap: (){
              setState((){
                _showPassword = ! _showPassword;
              });
            },
            child: Icon(
              _showPassword ? Icons.visibility : Icons.visibility_off,
              color: Colors.black,
            ),
          ),
          border: InputBorder.none,
        ),
      ),
    );
  }
}
