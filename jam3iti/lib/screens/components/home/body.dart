import 'package:flutter/material.dart';
import 'package:sportsapp/screens/components/home/background.dart';
import 'package:sportsapp/screens/components/my_buttons_inputs_fields/rounded_button.dart';
import 'package:sportsapp/screens/components/authenticate/login/login_screen.dart';
import 'package:sportsapp/screens/components/authenticate/signup/signup_screen.dart';

class Body extends StatelessWidget {
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Background(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "Welcome to Jam3iti",
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(
              height: size.height * 0.04,
            ),
            Image.asset(
              "assets/images/oulidha.png",
              height: size.height * 0.4,
            ),
            SizedBox(
              height: size.height * 0.06,
            ),
            RoundedButton(
              text: "LOGIN",
              textColor: Colors.black,
              press: (){
                Navigator.push(context,
                  MaterialPageRoute(builder: (context){
                    return LoginScreen();
                    },
                  ),
                );},
            ),
            RoundedButton(
              text: "SINGUP",
              color: Colors.red,
              textColor: Colors.black,
              press: (){
                Navigator.push(
                context, MaterialPageRoute(
                builder: (context){
                return SignUpScreen();
              },
            ),
            );},
            ),
          ],
        ),
      ),
    );
  }
}


