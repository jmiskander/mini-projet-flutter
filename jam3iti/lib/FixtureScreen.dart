import 'package:flutter/material.dart';
import 'CAFCL.dart';
import 'CoupeTun.dart';
import 'Ligue1tun.dart';


class FixtureScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 6,
      child: Scaffold(
        appBar: AppBar(
          bottom: PreferredSize(
            child: TabBar(
              isScrollable: true,
              unselectedLabelColor: Colors.white.withOpacity(0.3),
              indicatorColor: Colors.white,
              tabs: [
                Tab(
                  child: Text('Today'),
                ),
                Tab(
                  child: Text('10 Nov'),
                ),
                Tab(
                  child: Text('11 Nov'),
                ),
                Tab(
                  child: Text('12 Nov'),
                ),
                Tab(
                  child: Text('13 Nov'),
                ),
                Tab(
                  child: Text('14 Nov'),
                ),
              ],
            ),
            preferredSize: Size.fromHeight(20.0),
          ),
        ),
        body: SingleChildScrollView(
          child: SizedBox(
            height: 2000,
            child: TabBarView(
              children: [
              Padding(
                padding: const EdgeInsets.only(left: 13.0, right: 13.0, bottom: 13.0, top: 25),
                child: Column(
                    children: [
                      CoupeTun(),
                      Ligue1tun(),
                    ],
                  ),
              ),
                Padding(
                  padding: const EdgeInsets.only(left: 13.0, right: 13.0, bottom: 13.0, top: 25),
                  child: Column(
                    children: [
                      CoupeTun(),
                      Ligue1tun(),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 13.0, right: 13.0, bottom: 13.0, top: 25),
                  child: Column(
                    children: [
                      CoupeTun(),
                      Ligue1tun(),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 13.0, right: 13.0, bottom: 13.0, top: 25),
                  child: Column(
                    children: [
                      CoupeTun(),
                      Ligue1tun(),
                      CAFCL(),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 13.0, right: 13.0, bottom: 13.0, top: 25),
                  child: Column(
                    children: [
                      CoupeTun(),
                      Ligue1tun(),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 13.0, right: 13.0, bottom: 13.0, top: 25),
                  child: Column(
                    children: [
                      CoupeTun(),
                      Ligue1tun(),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
        ),
    );
  }
}
