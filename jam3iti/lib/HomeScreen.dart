import 'package:flutter/material.dart';
import 'package:sportsapp/FixtureScreen.dart';
import 'package:sportsapp/MainScreen.dart';
import 'package:sportsapp/ProfileScreen.dart';
import 'package:sportsapp/screens/drawer/drawer_header.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  var currentPage = DrawerSections.home;

  int _selectedIndex = 0;
  List<Widget> _widgetOptions = <Widget>[
    MainScreen(),
    FixtureScreen(),
    ProfileScreen(),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        body: _widgetOptions.elementAt(_selectedIndex),
        bottomNavigationBar: BottomNavigationBar(
          backgroundColor: Colors.grey[800],
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: 'Home',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.calendar_today),
              label: 'Fixtures',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.person),
              label: 'Profile',
            ),
          ],
          currentIndex: _selectedIndex,
          selectedItemColor: Colors.amber,
          onTap: _onItemTapped,
        ),
        drawer: Drawer(
          child: SingleChildScrollView(
            child: Container(
              child: Column(
                children: [
                  HeaderDrawer(),
                  DrawerItemsList(),
                ],
              ),
            ),
          ),
        ),
      );
}

Widget DrawerItemsList() {
  var currentPage = DrawerSections.home;
  return Container(
    padding: EdgeInsets.only(
      top: 15,
    ),
    child: Column(
        // Show list of Menus in the drawer
        children: [
          MenuItem(1,"Home", Icons.home_outlined,
          currentPage == DrawerSections.home ? true : false),
          MenuItem(2,"Fixtures", Icons.event,
          currentPage == DrawerSections.fixtures ? true : false),
          MenuItem(3,"Language Changer", Icons.settings_outlined,
              currentPage == DrawerSections.language_changer ? true : false),
          MenuItem(4,"Notifications", Icons.notifications_outlined,
              currentPage == DrawerSections.notifications ? true : false),
          MenuItem(5,"Privacy Policy", Icons.privacy_tip_outlined,
              currentPage == DrawerSections.privacy_policy ? true : false),
          MenuItem(6,"Send Feedback", Icons.feedback_outlined,
              currentPage == DrawerSections.send_feedback ? true : false),
          MenuItem(7,"Logout", Icons.lock,
              currentPage == DrawerSections.send_feedback ? true : false),
        ],
    ),
  );
}

Widget MenuItem(int id, String title, IconData icon, bool selected) {
  return Material(
    child: InkWell(
      onTap: () {},
      child: Padding(
        padding: EdgeInsets.all(15.0),
        child: Row(
          children: [
            Expanded(
              child: Icon(
                icon,
                size: 20,
                color: Colors.white70,
              ),
            ),
            Expanded(
              flex: 3,
              child: Text(
                      title,
                      style: TextStyle(
                      color: Colors.white70,
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ],
        ),
      ),
    ),
  );
}
enum DrawerSections {
  home,
  fixtures,
  language_changer,
  notifications,
  privacy_policy,
  send_feedback,
  logout,
}