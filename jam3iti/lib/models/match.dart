class MatchModel {
  int id;
  String homeClub;
  String awayClub;
  String homeClubLogo;
  String awayClubLogo;
  int homeGoals;
  int awayGoals;
  String date;

  MatchModel({
    this.id,
    this.homeClub,
    this.awayClub,
    this.homeGoals,
    this.homeClubLogo,
    this.awayClubLogo,
    this.awayGoals,
    this.date,
  });
}